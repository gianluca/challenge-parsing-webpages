#!/usr/bin/python3
import unittest
import scrape as s

class Test(unittest.TestCase):
    def test_is_valid_url(self):
        self.assertTrue(s.is_valid_url("foo"))
        self.assertTrue(s.is_valid_url("123"))
        self.assertTrue(s.is_valid_url("foo.foo"))
        self.assertTrue(s.is_valid_url("http://foo.foo"))
        self.assertTrue(s.is_valid_url("https://foo.foo"))

        self.assertFalse(s.is_valid_url(None))
        self.assertFalse(s.is_valid_url(""))
        self.assertFalse(s.is_valid_url("ftp://foo"))
        self.assertFalse(s.is_valid_url("foo://foo"))
        self.assertFalse(s.is_valid_url("123://foo"))
        self.assertFalse(s.is_valid_url("://foo"))

    def test_get_image_absolute_url(self):
        self.assertEqual(s.get_image_absolute_url(None, 'foo', 'foo'), None)
        self.assertEqual(s.get_image_absolute_url('foo', None, 'foo'), None)
        self.assertEqual(s.get_image_absolute_url('foo', 'foo', None), None)

        self.assertEqual(s.get_image_absolute_url('http://a', 'http://a', 'img'), 'http://a/img')
        self.assertEqual(s.get_image_absolute_url('http://a', 'http://a', '/img'), 'http://a/img')
        self.assertEqual(s.get_image_absolute_url('http://a', 'http://a', '//b/img'), 'http://b/img')
        self.assertEqual(s.get_image_absolute_url('http://a', 'http://a', 'http://b/img'), 'http://b/img')

        self.assertEqual(s.get_image_absolute_url('http://a/b', 'http://a', 'img'), 'http://a/b/img')
        self.assertEqual(s.get_image_absolute_url('http://a/b', 'http://a', '/img'), 'http://a/img')
        self.assertEqual(s.get_image_absolute_url('http://a/b', 'http://a', '//b/img'), 'http://b/img')
        self.assertEqual(s.get_image_absolute_url('http://a/b', 'http://a', 'http://b/img'), 'http://b/img')

    def test_get_image_extension(self):
        self.assertEqual(s.get_image_extension(None, 'foo'), None)
        self.assertEqual(s.get_image_extension('foo', None), None)

        self.assertEqual(s.get_image_extension('image/jpg', 'foo'), 'jpg')
        self.assertEqual(s.get_image_extension('image/jpg', 'foo.png'), 'jpg')
        self.assertEqual(s.get_image_extension('foo/foo', 'foo.png'), 'png')
        self.assertEqual(s.get_image_extension('foo/foo', 'foo.foo'), 'foo')
        self.assertEqual(s.get_image_extension('foo/foo', 'foo.123'), '123')
        self.assertEqual(s.get_image_extension('foo/foo', 'foo.123456'), '123456')
        self.assertEqual(s.get_image_extension('foo/foo', 'foo'), 'unknown')
        self.assertEqual(s.get_image_extension('foo/foo', '123'), 'unknown')
        self.assertEqual(s.get_image_extension('foo/foo', 'foo.'), 'unknown')
        self.assertEqual(s.get_image_extension('foo/foo', 'foo.//'), 'unknown')

if __name__ == '__main__':
    unittest.main()
