# Webpage images scraper

## Description

Write a program in Python which:
- Finds and fetches all images on a webpage and stores the images on disk
- Writes a file on disk, which lists the URL's of the images fetched.

## Usage

```sh
# only http/https urls are supported
./scrape.py [--debug] <url>
```
