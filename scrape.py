#!/usr/bin/python3

import sys, os, time, re, datetime, shutil, urllib.request
from html.parser import HTMLParser

TIMESTAMP = datetime.datetime.fromtimestamp(
				time.time()).strftime('%Y%m%d%H%M%S')

SUPPORTED_EXTENSIONS = {
	'image/jpg': 'jpg',
	'image/jpeg': 'jpg',
	'image/png': 'png',
	'image/gif': 'gif'
}

SUPPORTED_SCHEMES = ['http', 'https']

DEFAULT_SCHEME = SUPPORTED_SCHEMES[0]

DEFAULT_CHARSET = 'utf-8'

# modified user agent to allow requests to sites blocking scripts (e.g. google)
# https://docs.python.org/3.4/library/urllib.request.html#urllib.request.Request
USER_AGENT = "Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0"

class Camera:
	def __init__(self, args):
		self.url = None # always prefixed with scheme (default is 'http://')
		self.size = 0 # number of images found
		self.dbg = False # debug flag for extra messages

		self.run(args)

	def read_arguments(self, args):
		status = False
		arguments = args[1:]

		while (len(arguments) > 0):
			if (arguments[0] == "--debug"):
				self.dbg = True
				arguments = arguments[1:]
			elif (not self.url and is_valid_url(arguments[0])):
				# add default scheme if missing
				if ('://' in arguments[0]):
					self.url = arguments[0]
				else:
					self.url = DEFAULT_SCHEME + '://' + arguments[0]

				arguments = arguments[1:]
				status = True
			else:
				status = False
				arguments = []

		return status

	def run(self, args):
		if not self.read_arguments(args):
			print_usage()
			return

		print('Loading page... ', end='', flush=True)

		page = request(self.url)
		if not page:
			return

		if self.dbg: print(page.headers)

		# read page
		charset = page.headers.get_content_charset()
		page_html = None
		try:
			page_html = page.read().decode(
				charset if charset else DEFAULT_CHARSET)
		except Exception as e:
			print("unexpected error reading page ({}: {})".format(
				type(e).__name__, str(e)))
			return

		# run the parser
		parser = Parser()
		parser.feed(page_html)
		parser.close()
		self.size = len(parser.srcs)

		print('complete. {} {} found.'.format(
			self.size,
			'images' if self.size > 1 else 'image'
		))
		print('Loading images', end='', flush=True)

		# prepare output folder
		os.makedirs(TIMESTAMP)
		log_out = open('{}/log.out'.format(TIMESTAMP), 'w')

		# loop through every image found and fetch
		file_counter = 0
		for src in parser.srcs:
			# first build the absolute path to the image resource based
			# on the src html attribute

			array = self.url.split('/')
			# everything but what's after the last '/'
			page_url = '/'.join(array[:max(3, len(array) - 1)])
			# website domaine (everything up to the 3rd '/')
			root_url = '/'.join(array[:3])

			image_url = get_image_absolute_url(page_url, root_url, src)

			if self.dbg: print('\nimage {}'.format(file_counter + 1))
			if self.dbg: print('src: {}'.format(src))
			if self.dbg: print('image_url: {}'.format(image_url))

			image = request(image_url)
			if not image:
				if self.dbg: print("failed to load {}".format(image_url))
				continue

			# finally, we fetch image content and write to file
			# we avoid using urlretrieve since it appears to be legacy
			# https://docs.python.org/dev/library/urllib.request.html#legacy-interface
			file_counter += 1
			file_extension = get_image_extension(
				image.headers['Content-Type'], image_url)
			file_name = '{}/{}.{}'.format(
				TIMESTAMP,
				str(file_counter).zfill(len(str(self.size))),
				file_extension)
			out = open(file_name, 'wb')
			shutil.copyfileobj(image, out)
			log_out.write('{}\t{}\n'.format(file_name, image_url))
			out.close()

			print('.', end="", flush=True)

		log_out.close()
		print('\nDone. Saved {}/{} {} to {} (see log.out for details).'.format(
			file_counter,
			self.size,
			'images' if (file_counter > 1) else 'image',
			TIMESTAMP))

# HTML parser
class Parser(HTMLParser):
	def __init__(self):
		HTMLParser.__init__(self)

		# contains src attributes of all <img>
		self.srcs = []

	def handle_starttag(self, tag, attributes):
		if (tag == 'img'):
			for name, value in attributes:
				if (name == 'src'): self.srcs.append(value)

# returns an object representing the HTTP response, or None
def request(url):
	response = None

	try:
		response = urllib.request.urlopen(urllib.request.Request(
			url, data = None, headers = {'User-Agent': USER_AGENT}
		))
	except Exception as e:
		print("failed to open url ({}: {})".format(type(e).__name__, str(e)))

	return response

# url is valid if provided scheme is supported, or is missing in which case the
# default is 'http'
def is_valid_url(url):
	if not url:
		# invalid input
		return False

	isValid = False

	array = url.split('://')
	if (len(array) == 1):
		isValid = True
	elif (array[0] in SUPPORTED_SCHEMES):
		isValid = True

	return isValid

# determines absolute path of resource src
def get_image_absolute_url(page_url, root_url, src):
	if (not page_url or not root_url or not src):
		# invalid input
		return None

	image_url = None

	if (src.startswith('http://') or src.startswith('https://')):
		# absolute url
		image_url = src
	elif (src.startswith('//')):
		# absolute url with same scheme as page
		image_url = page_url.split('://')[0] + ':' + src
	elif (src.startswith('/')):
		# relative url from root
		image_url = root_url + src
	else:
		# relative url from page location
		image_url = page_url + '/' + src

	# remove whitespaces
	image_url = image_url.replace(" ", "")

	return image_url

def get_image_extension(content_type, image_url):
	if (not content_type or not image_url):
		# invalid input
		return None

	extension = None

	# finds out the image file extension, first from the http header, failing
	# that from the path itself, failing that just mark it 'unknown'
	if (content_type in SUPPORTED_EXTENSIONS):
		extension = SUPPORTED_EXTENSIONS.get(content_type)
	else:
		match = re.match(r'.*\.(\w+)$', image_url);
		if (match):
			extension = match.group(1)
		else:
			extension = 'unknown'

	return extension

def print_usage():
	print("Usage:\tpython3 scrape.py [--debug] <url>");
	print("\t(only http/https urls are supported)")

def main(args):
    Camera(args)

if __name__ == '__main__':
    main(sys.argv)
